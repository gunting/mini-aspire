# Loan Management System

This is a simple loan management system that allows users to apply for a loan, view their loan details, and make loan repayments. 

## Prerequisites

Before running the application, make sure you have the following:

- PHP >= 7.3
- Composer
- MySQL

## Installation

1. Clone the repository to your local machine:

```
git clone https://github.com/your-username/loan-management-system.git
```

2. Change into the project directory:

```
cd loan-management-system
```

3. Install the application dependencies using Composer:

```
composer install
```

4. Create a new `.env` file:

```
cp .env.example .env
```

5. Generate a new `APP_KEY`:

```
php artisan key:generate
```

6. Configure the `.env` file with your database credentials:

```
DB_DATABASE=your_database_name
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password
```

7. Migrate the database:

```
php artisan migrate
```

8. Seed the database with test data:

```
php artisan db:seed
```

## Usage

1. Start the PHP development server:

```
php artisan serve
```

2. We need to login first to start use the program with this curl
```
curl --location 'http://localhost:8000/api/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "{{email}}",
    "password": "{{password}}"
}'
```

3. To login as an admin, use the following credentials:

```
Email: admin@admin.com
Password: asdasdasd
```

4. To login as a regular user, use the following credentials:

```
Email: asdasd@asdasd.com
Password: asdasdasd
Email: qweqwe@qweqwe.com
Password: asdasdasd
```

5. Once logged in, you can view your loan details, apply for a loan, and make loan repayments.

6. To see the loan list use this curl(admin can see all, use can only see their loan)
```
curl --location --request GET 'http://localhost:8000/api/loans' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer {{access_token from login response}}' 
```

7. To start loan use this API
```
curl --location 'http://localhost:8000/api/loans' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer {{access_token from login response}}' \
--data '{
        "amount": {{amount of loan in numeric}},
        "term": {{term of payment in numeric}}
    }'
```

8. To approve loan use this API, only admin can approve the loan
```
curl --location --request PUT 'http://localhost:8000/api/loans/{{loan_id}}/approve' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer {{access_token from login response}}'
```

9.  To check repayment list based on loan_id use this API
```
curl --location --request GET 'http://localhost:8000/api/loans/{{loan_id}}/repayments' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer {{access_token from login response}}'
```

10.  To pay based on repayment_id use this API
```
curl --location 'http://localhost:8000/api/repayments/{{repayment_id}}/pay' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer {{access_token from login response}}' \
--data '{
        "amount": {{amount of payment in numeric}}
    }'
```

## License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).