<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $admin = new User();
        $admin->email = 'admin@admin.com';
        $admin->password = Hash::make('asdasdasd');
        $admin->role = 'admin';
        $admin->save();

        $user = new User();
        $user->email = 'asdasd@asdasd.com';
        $user->password = Hash::make('asdasdasd');
        $user->role = 'user';
        $user->save();
        
        $user = new User();
        $user->email = 'qweqwe@qweqwe.com';
        $user->password = Hash::make('asdasdasd');
        $user->role = 'user';
        $user->save();
    }
}
