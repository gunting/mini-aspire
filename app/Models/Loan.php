<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Loan extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'status', 'term', 'user_id', 'start_date'];
    
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_PAID = 'paid';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function repayments()
    {
        return $this->hasMany(Repayment::class);
    }

    public function generateRepayments()
    {
        $repayments = [];
        $loanAmount = $this->amount;
        $termAmount = round($loanAmount / $this->term);
        $remainingAmount = $loanAmount;

        for ($i = 1; $i <= $this->term; $i++) {
            $dueDate = Carbon::parse($this->start_date)->addDays($i * 7);
            $repaymentAmount = ($i == $this->term) ? $remainingAmount : $termAmount;
            $remainingAmount -= $repaymentAmount;

            $repayments[] = [
                'loan_id' => $this->id,
                'due_date' => $dueDate,
                'amount' => $repaymentAmount,
                'status' => Loan::STATUS_PENDING,
                // 'created_at' => Carbon::now(),
            ];
        }

        return $this->repayments()->createMany($repayments);
    }
}