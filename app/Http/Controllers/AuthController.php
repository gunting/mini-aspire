<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $test = $request->bearerToken();
    
        if (Auth::attempt($credentials)) {
            $accessToken = Auth::user()->createToken('MyApp')->plainTextToken;
            return response()->json(['access_token' => $accessToken]);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
