<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\Repayment;
use Carbon\Carbon;

class RepaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, $loanId)
    {
        $userId = auth()->user()->id;
        if (!auth()->user()->hasRole('admin')) {
            try {
                $loan = Loan::where('id', $loanId)
                            ->where('user_id', $userId)
                            ->firstOrFail();
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json(['error' => 'Loan not found'], 404);
            }
        }

        $repayments = Repayment::where('loan_id', $loanId)
            ->orderBy('due_date')
            ->get();

        return response()->json([
            'repayments' => $repayments,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Loan $loan)
    {
       
    }

    public function generateRepayments()
    {
        $repayments = [];
        $loanAmount = $this->amount;
        $loanTerm = $this->term;
        $repaymentAmount = ceil($loanAmount / $loanTerm); // Round up repayment amount to avoid underpayment
        $startDate = Carbon::parse($this->start_date);
        $lastRepaymentAmount = $loanAmount - ($repaymentAmount * ($loanTerm - 1)); // Calculate last repayment amount if there's a remainder
        
        for ($i = 0; $i < $loanTerm; $i++) {
            $dueDate = $startDate->copy()->addDays(7 * ($i + 1));
            if ($i == $loanTerm - 1) {
                $repaymentAmount = $lastRepaymentAmount;
            }
            $repayments[] = [
                'loan_id' => $this->id,
                'due_date' => $dueDate,
                'amount' => $repaymentAmount,
                'paid' => false
            ];
        }

        $this->repayments()->createMany($repayments);
    }

    public function getNextRepayment($loanId)
    {
        $repayment = Repayment::where('loan_id', $loanId)
                            ->where('status', Loan::STATUS_PENDING)
                            ->orderBy('due_date')
                            ->first();
        return $repayment;
    }

    public function payLoan($loanId)
    {
        $loan = Loan::where('id', $loanId)->firstOrFail();
                $loan->status = Loan::STATUS_PAID;
                $loan->save();
        return $loan;
    }

    public function pay(Request $request, $repaymentId)
    {
        $userId = $request->user()->id;

        try {
            $repayment = Repayment::where('id', $repaymentId)
                                ->whereHas('loan', function ($query) use ($userId) {
                                    $query->where('user_id', $userId);
                                })
                                ->where('status', Loan::STATUS_PENDING)
                                ->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Repayment not found'], 404);
        }

        $amount = $request->input('amount');
        $overpaid = false;

        if ($amount < $repayment->amount) {
            // Payment is less than the scheduled repayment
            return response()->json([
                'message' => 'Payment amount is less than the scheduled repayment amount'
            ], 400);
        } else if ($amount >= $repayment->amount) {
            // If payment > loan we get all the pending repayment and validate it with inputted amount
            $pendingAmount = 0;
            $pendingRepayments = Repayment::where('status', Loan::STATUS_PENDING)->get();

            foreach ($pendingRepayments as $tempRepayment) {
                $pendingAmount += $tempRepayment->amount;
            }

            if ($pendingAmount < $amount) {
                return response()->json([
                    'message' => "Payment amount is bigger than pending amount, your pending amount is {$pendingAmount}"
                ], 400);
            }


            // Payment is enough to cover the full repayment
            $repayment->status = Loan::STATUS_PAID;
            $repaymentOriginalAmount = $repayment->amount;
            $repayment->amount = $amount;
            $repayment->paid_date = Carbon::now();
            $repayment->save();

            // Check next repayment if exist, if not then mark as paid
            $nextRepayment = $this->getNextRepayment($repayment->loan_id);

            if (!$nextRepayment) {
                $this->payLoan($repayment->loan_id);

                return response()->json([
                    'message' => 'Payment successful'
                ]);
            }

            // Check if there is any overpayment
            if ($amount > $repaymentOriginalAmount) {
                $overpaid = true;
                $overpaidAmount = $amount - $repaymentOriginalAmount;

                while ($overpaid) {
                    // Find the next pending repayment for the loan
                    $nextRepayment = $this->getNextRepayment($repayment->loan_id);
                    if ($nextRepayment) {
                        // Reduce the next repayment amount by the overpaid amount
                        if ($overpaidAmount < $nextRepayment->amount) {
                            // The overpaid amount can cover the next repayment
                            $nextRepayment->amount -= $overpaidAmount;
                            $nextRepayment->save();

                            break;
                        } else {
                            // The overpaid amount is greater than the next repayment, mark it as paid
                            $nextRepayment->status = Loan::STATUS_PAID;
                            $nextRepayment->paid_date = Carbon::now();
                            $repaymentOriginalAmount = $nextRepayment->amount;
                            $nextRepayment->amount = 0;
                            $nextRepayment->save();

                            // Calculate the remaining overpaid amount
                            $overpaidAmount = $overpaidAmount - $repaymentOriginalAmount;
                        }
                    } else {
                        $this->payLoan($repayment->loan_id);

                        break;
                    }
                }
            }
        }

        return response()->json([
            'message' => 'Payment successful'
        ]);
    }

}
