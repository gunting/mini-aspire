<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\Repayment;

class LoansController extends Controller
{
    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        // Validate the input
        $request->validate([
            'amount' => 'required|numeric|min:1',
            'term' => 'required|integer|min:1',
        ]);

        // Create a new loan for the current user
        $loan = new Loan([
            'amount' => $request->input('amount'),
            'term' => $request->input('term'),
            'user_id' => auth()->user()->id,
            'status' => Loan::STATUS_PENDING,
            'start_date' => now()->toDateString(),
        ]);
        $loan->save();

        // // Generate scheduled repayments
        // $schedule = $loan->generateRepaymentSchedule();

        // Return the new loan and its schedule
        return response()->json([
            'loan' => $loan,
            // 'schedule' => $schedule,
        ]);
    }


    /**
     * Display the specified resource.
     */
    public function show()
    {
        if (auth()->user()->hasRole('admin')) {
            $loans = Loan::all();
        } else {
            $loans = Loan::where('user_id', auth()->id())->get();
        }

        return response()->json(['loans' => $loans]);
    }

    public function approveLoan($id)
    {
        if (!auth()->user()->hasRole('admin')) {
            return response()->json(['message' => 'Only admin can approve loan']);
        }

        $loan = Loan::where('id', $id)
                    ->where('status', Loan::STATUS_PENDING)
                    ->first();
        if ($loan) {
            $loan->status = Loan::STATUS_APPROVED;
            $loan->save();

            // Generate repayments
            $repayments = $loan->generateRepayments();
            // var_dump($repayments);
            // die();
            $repaymentArray = $repayments->toArray();
            // Repayment::insert($repaymentArray);

            return response()->json(['message' => 'Loan approved successfully']);
        } else {
            return response()->json(['error' => 'Loan not found'], 404);
        }
    }
}
